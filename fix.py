# -*- coding: utf-8 -*-

"""
Author  : Yuuki-chan
Created : 2020/05/15 @ 12:10 AM JST
Updated : 2020/05/15 @ 02:00 AM JST
Version : 0.1.1
"""

import argparse
import json
import os

fp = os.path.realpath(__file__)
cwd = os.path.dirname(fp)

parser = argparse.ArgumentParser()
parser.add_argument('--input', '-i', required=True, help='Input .ass')
args = parser.parse_args()

if __name__ == '__main__':
    ln = 1
    fixed_ass = ''
    ass1 = ...
    ass2 = []

    DCRS = json.load(open(f'{cwd}/DCRS.json', encoding='UTF-8'))

    # first pass
    with open(f'{cwd}/{args.input}', 'r', encoding='UTF-8') as ass_file:
        ass1 = ass_file.readlines()

        for line in ass1:
            ln += 1

            if '[外:' in line:
                OLD_DCR = line[line.find('[')+3:line.find(']')]
                NEW_DCR = ''

                try:
                    NEW_DCR = DCRS[OLD_DCR]
                except Exception as e:
                    NEW_DCR = OLD_DCR

                print(f'[1st pass] Fixed "{OLD_DCR}" -> "{NEW_DCR}" on line #{ln}')

                ass2.append(line.replace(f'[外:{OLD_DCR}]', NEW_DCR))
            else:
                ass2.append(line)

    # second pass
    for line in ass2:
        if '[外:' in line:
            OLD_DCR = line[line.find('[') + 3:line.find(']')]
            NEW_DCR = ''

            try:
                NEW_DCR = DCRS[OLD_DCR]
            except Exception as e:
                NEW_DCR = OLD_DCR

            print(f'[2nd pass] Fixed "{OLD_DCR}" -> "{NEW_DCR}" on line #{ln}')

            fixed_ass += line.replace(f'[外:{OLD_DCR}]', NEW_DCR)
        else:
            fixed_ass += line

    # write fixed .ass
    if str(f'{cwd}/{args.input}').endswith('.ass'):
        with open(str(f'{cwd}/{args.input}').replace('.ass', '_fixed.ass'), 'w', encoding='UTF-8') as ass_file:
            ass_file.write(fixed_ass)
    elif str(f'{cwd}/{args.input}').endswith('.srt'):
        with open(str(f'{cwd}/{args.input}').replace('.srt', '_fixed.srt'), 'w', encoding='UTF-8') as ass_file:
            ass_file.write(fixed_ass)
